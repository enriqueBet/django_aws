import boto3

def create_folder(bucket, directory_name):
    """Crea un directorio en un bucket de S3"""
    try:
        s3 = boto3.client('s3')
        key = f"{directory_name}/"
        s3.put_object(Bucket=bucket, Key=key)
        return key
    except Exception as error:
        print(error)

    return None

def upload_image(bucket, mediafile_key, file):
    """"
    Upload a image into S3
    
    :param bucket: S3 Bucket were the image will be uploaded
    :param mediafile_key: Full path to the image inside the bucket
    :param file: The image to upload
    """
    try:
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket)
        bucket.put_object(
            ACL='public-read',
            Key=mediafile_key,
            ContentType=file.content_type,
            Body=file
        )
        return True
    except Exception as error:
        print(error)

    return False

def rename_file(bucket, new_mediafile_key, old_mediafile_key):
    try:
        s3 = boto3.resource('s3')
        s3.Object(bucket, new_mediafile_key).copy_from(CopySource=f"{bucket}/{old_mediafile_key}")
        s3.Object(bucket, old_mediafile_key).delete()
        # Modificacion de permisos
        s3.Object(bucket, new_mediafile_key).Acl().put(ACL="public-read")
        return True
    except Exception as error:
        print(error)

def delete_file(bucket, key):
    try:
        s3 = boto3.resource('s3')
        s3.Object(bucket, key).delete()
        return True
    except Exception as error:
        print(error)

def get_mediafile_content(bucket, key):
    try:
        s3 = boto3.client('s3')
        data = s3.get_object(Bucket=bucket, Key=key)
        return data['Body'].read() if data else None

    except Exception as error:
        print(error)

def download_file(bucket, key, local_path):
    try:
        s3 = boto3.client('s3')
        s3.download_file(bucket, key, local_path)

    except Exception as error:
        print(error)

