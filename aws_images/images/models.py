import zipfile
from io import BytesIO

from django.db import models
from django.http import HttpResponse
from albums.models import Album
from AWS import rename_file
from AWS import delete_file
from AWS import get_mediafile_content


class ImageManager(models.Manager):

    def delete_by_aws(self, id):
        image = self.filter(id=id).first()

        if image and delete_file(image.bucket, image.key):
            image.delete()
            return id

    def download_and_zip(self, ids):
        zip_io = BytesIO()
        with zipfile.ZipFile(zip_io, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_writter:
            for id in ids:
                image = self.filter(id=id).first()
                data = get_mediafile_content(image.bucket, image.key)
                if image and data:
                    zip_writter.writestr(image.name, data)
                         
        return zip_io

class Image(models.Model):
    key = models.CharField(max_length=100, null=False, blank=False)
    bucket = models.CharField(max_length=100, null=False, blank=False)
    name = models.CharField(max_length=50, null=False, blank=False)
    content_type = models.CharField(max_length=20, null=False, blank=False)
    size = models.IntegerField()
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    create_at = models.DateTimeField(auto_now_add=True)

    objects = ImageManager()

    def __str__(self):
        return self.name

    @property
    def url(self):
        return f"https://{self.bucket}.s3.amazonaws.com/{self.key}"

    @property
    def title(self):
        return self.name.split(".")[0] if "." in self.name else self.name

    @property
    def extension(self):
        return self.name.split(".")[-1] if "." in self.name else ""

    def set_name(self, value):
        """set a new name"""
        name = f"{value}.{self.extension}" if self.extension else value
        key = f"{self.album.key}{name}"

        if rename_file(bucket=self.bucket, new_mediafile_key=key, old_mediafile_key=self.key):
            self.key = key
            self.name = name
            self.save()

    def delete_image(self):
        """delete the image from the data base and from AWS"""
        if delete_file(bucket=self.bucket, key=self.key):
            print(f"Deleting image {self.name}")
            self.delete()
            return True
        print(f"Deletion was unsuccessful for image {self.name}")
        
