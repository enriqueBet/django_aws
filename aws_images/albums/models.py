from django.db import models
from AWS import create_folder


class AlbumManager(models.Manager):

    def create_by_aws(self, bucket, title, description):
        key = create_folder(bucket, title.replace(' ', '').lower())
        if key:
            return self.create(title=title, description=description, 
                               bucket=bucket, key=key)
        return None


class Album(models.Model):
    """pass"""
    title = models.CharField(max_length=50, null=False, blank=False) # con null=False, blank=False se le de ha entnder a django que este campo es requerido
    description = models.TextField()
    bucket = models.CharField(max_length=100, null=False, blank=False) # Se usa para conocer la ruta exacta del bucket de S3
    key = models.CharField(max_length=50, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True) # Agrega la fecha automaticamente

    objects = AlbumManager()

    def __str__(self):
        return self.title

    @property
    def images(self):
        return self.image_set.all()
